package org.evolvis.util.simplepaging.test;

import java.util.ArrayList;
import java.util.List;

public class Util {

    public static List createMockData(int size) {

        List<Integer> data = new ArrayList<Integer>();
        for (int i = 1; i <= size; i++) {
            data.add(i);
        }
        return data;
    }
}
