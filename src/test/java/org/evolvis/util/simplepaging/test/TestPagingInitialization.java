package org.evolvis.util.simplepaging.test;

import org.evolvis.util.simplepaging.DumbestPossibleDataProvider;
import org.evolvis.util.simplepaging.SimplePagingFactory;
import org.evolvis.util.simplepaging.SimplePagingObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestPagingInitialization {

    @Test
    public void testCreateMockData() {
        List<Object> data = Util.createMockData(3);
        Assert.assertEquals(3, data.size());
    }

    @Test
    public void testDataProvider() {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(3));

        List data = dataProvider.getData(0, 3);
        Assert.assertEquals(3, data.size());

        data = dataProvider.getData(0, 1);
        Assert.assertEquals(1, data.size());
        Assert.assertEquals(1, data.get(0));

        data = dataProvider.getData(1, 2);
        Assert.assertEquals(2, data.size());
        Assert.assertEquals(2, data.get(0));
    }

    @Test
    public void testSingleItem() throws Exception {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(1));
        SimplePagingObject pagingObject = SimplePagingFactory.createPagingObject(dataProvider, 20, 1);
        Assert.assertEquals(1, pagingObject.getTotalPages());
        Assert.assertEquals(1, pagingObject.getTotalItems());
        Assert.assertEquals(1, pagingObject.getDisplayFrom());
        Assert.assertEquals(1, pagingObject.getDisplayUntil());
        Assert.assertEquals(1, pagingObject.getData().size());
        Assert.assertEquals(1, pagingObject.getCurrentPage());
    }

    @Test
    public void testFullPage() throws Exception {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(10));
        SimplePagingObject pagingObject = SimplePagingFactory.createPagingObject(dataProvider, 10, 1);
        Assert.assertEquals(1, pagingObject.getTotalPages());
        Assert.assertEquals(10, pagingObject.getTotalItems());
        Assert.assertEquals(1, pagingObject.getDisplayFrom());
        Assert.assertEquals(10, pagingObject.getDisplayUntil());
        Assert.assertEquals(10, pagingObject.getData().size());
        Assert.assertEquals(1, pagingObject.getCurrentPage());
    }

    @Test
    public void testThreePagesFirstPage() throws Exception {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(21));
        SimplePagingObject pagingObject = SimplePagingFactory.createPagingObject(dataProvider, 10, 1);

        Assert.assertEquals(3, pagingObject.getTotalPages());
        Assert.assertEquals(21, pagingObject.getTotalItems());

        Assert.assertEquals(1, pagingObject.getDisplayFrom());
        Assert.assertEquals(10, pagingObject.getDisplayUntil());

        Assert.assertEquals(1, pagingObject.getCurrentPage());

        List data = pagingObject.getData();
        Assert.assertEquals(10, data.size());
        Assert.assertEquals(1, data.get(0));
        Assert.assertEquals(10, data.get(9));
    }

    @Test
    public void testThreePagesSecondPage() throws Exception {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(21));
        SimplePagingObject pagingObject = SimplePagingFactory.createPagingObject(dataProvider, 10, 11);

        Assert.assertEquals(11, pagingObject.getDisplayFrom());
        Assert.assertEquals(20, pagingObject.getDisplayUntil());

        Assert.assertEquals(2, pagingObject.getCurrentPage());

        List data = pagingObject.getData();
        Assert.assertEquals(10, data.size());
        Assert.assertEquals(11, data.get(0));
        Assert.assertEquals(20, data.get(9));
    }

    @Test
    public void testThreePagesThirdPage() throws Exception {
        DumbestPossibleDataProvider dataProvider = new DumbestPossibleDataProvider(Util.createMockData(21));
        SimplePagingObject pagingObject = SimplePagingFactory.createPagingObject(dataProvider, 10, 21);

        Assert.assertEquals(21, pagingObject.getDisplayFrom());
        Assert.assertEquals(21, pagingObject.getDisplayUntil());

        Assert.assertEquals(3, pagingObject.getCurrentPage());

        List data = pagingObject.getData();
        Assert.assertEquals(1, data.size());
        Assert.assertEquals(21, data.get(0));
    }
}
