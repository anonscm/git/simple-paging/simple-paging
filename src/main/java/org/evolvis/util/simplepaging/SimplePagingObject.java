package org.evolvis.util.simplepaging;

import java.util.List;

/**
 * pojo you will wanna use as command object/to interoperate with the view part of your solution
 * dont worry instantiating it yourself. in controller, ask the SimplePagingFactory to create an instance for you.
 * from the view part let the mvc framework or whatever your using do it for you.
 */
public class SimplePagingObject {

    private int totalItems;
    private int itemsPerPage;
    private int displayFrom;
    private int displayUntil;
    private int currentPage;
    private int totalPages;
    private List<Object> data;

    public void setDisplayFrom(int displayFrom) {
        this.displayFrom = displayFrom;
    }

    public void setDisplayUntil(int displayUntil) {
        this.displayUntil = displayUntil;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getDisplayFrom() {
        return displayFrom;
    }

    public int getDisplayUntil() {
        return displayUntil;
    }

    public List<Object> getData() {
        return data;
    }
}
