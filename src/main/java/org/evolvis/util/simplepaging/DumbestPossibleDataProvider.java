package org.evolvis.util.simplepaging;

import java.util.List;

/**
 * The dumbest Implementation of a SimplePagingDataProvider one (I) could come up with
 * Always contains a complete list of items and returns a sublist on getData-calls
 */
public class DumbestPossibleDataProvider implements SimplePagingDataProvider {

    private List data;

    public DumbestPossibleDataProvider(List data) {
        this.data = data;
    }

    @Override
    public List getData(int startAt, int count) {

        // lets calculate until
        int until = startAt + count;

        // return data
        return data.subList(startAt, until);
    }

    @Override
    public int getTotalItemCount() {
        return data.size();
    }
}
