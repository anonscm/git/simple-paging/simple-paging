package org.evolvis.util.simplepaging;

import java.util.List;

/**
 * provides you with an easy as pie - static method that will give you a handy SimplePagingObject-instance
 */
public class SimplePagingFactory {

    /**
     * will provide you with a SimplePagingObject-instance if you throw in a dataprovider and how many items you
     * wanna display per page. also it needs to know wich item shall be the first to display
     *
     * @param dataProvider
     * @param itemsPerPage
     * @param startAt
     * @return
     */
    public static SimplePagingObject createPagingObject(SimplePagingDataProvider dataProvider, int itemsPerPage,
                                                        int startAt) {

        // pagingObject we want to build
        SimplePagingObject pagingObject = new SimplePagingObject();

        // set items per page
        pagingObject.setItemsPerPage(itemsPerPage);

        // total item count
        int totalItemCount = dataProvider.getTotalItemCount();

        // set count of total items/pages
        pagingObject.setTotalItems(totalItemCount);

        // if startAt is invalid, lets start at the beginning
        if (startAt > totalItemCount) {
            startAt = 1;
        }
        pagingObject.setDisplayFrom(startAt);

        // evaluate and set current page
        int currentPage = startAt / itemsPerPage;
        if (startAt % itemsPerPage > 0) {
            currentPage++;
        }
        pagingObject.setCurrentPage(currentPage);

        // evaluate and set until-value
        int until = startAt + itemsPerPage - 1;
        if (until > totalItemCount) {
            until = totalItemCount;
        }
        pagingObject.setDisplayUntil(until);

        // make sure we dont request anything that does not exist
        int dataProviderCount = itemsPerPage;
        if (startAt - 1 + itemsPerPage > totalItemCount) {
            dataProviderCount = totalItemCount - startAt + 1;
        }

        // get and set data
        List data = dataProvider.getData(startAt - 1, dataProviderCount);
        pagingObject.setData(data);

        // evaluate and set total amount of pages
        int totalPages = totalItemCount / itemsPerPage;
        if (totalItemCount % itemsPerPage > 0) {
            totalPages++;
        }
        pagingObject.setTotalPages(totalPages);

        return pagingObject;
    }
}
