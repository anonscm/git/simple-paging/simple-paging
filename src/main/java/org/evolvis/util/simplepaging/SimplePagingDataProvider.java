package org.evolvis.util.simplepaging;

import java.util.List;

/**
 * Implement this interface if you want to implement something clever as a data provider which only consumes
 * the data from a webservice you really need. Or caching. Or anything you want.
 */
public interface SimplePagingDataProvider {

    /**
     * returns a list of data beginning at the given offset containing as many items as specified in
     * the count parameter.
     *
     * @param offset - zero! based
     * @param count
     * @return
     */
    public List getData(int offset, int count);

    /**
     * returns information about how many items are in the collection (in total).
     *
     * @return
     */
    public int getTotalItemCount();
}
